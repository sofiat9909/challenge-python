# Python Challenge Repository

Welcome to the Python Challenge repository! This repository contains solutions for various Python programming challenges. Below is information about some of the included challenges.

# Gas Station Challenge

### Description
The Gas Station challenge involves finding the optimal starting gas station to travel around a circular route without running out of gas. Each gas station is represented by the amount of gas available and the gallons needed to reach the next station.

### Input
The input is an array, `strArr`, containing the number of gas stations `N` and details for each station in the format "g:c" where `g` is the amount of gas in gallons, and `c` is the gallons needed to reach the next station.

#### Example
["4", "3:1", "2:2", "1:2", "0:1"]

### Solution
The solution for this problem is in the `gas_station.py` file. Make sure to review the code and problem description to understand the implementation.


# Age Counting Challenge

### Description
The Age Counting challenge involves performing a GET request on a given API route containing data in the format: `key=STRING, age=INTEGER`. The goal is to count how many items exist with an age equal to or greater than 50 and print this final count.

### Solution
The solution for this problem is in the `age_counting.py` file. Make sure to review the code and problem description to understand the implementation.

### Task
Write a Python program to perform a GET request on the route [https://coderbyte.com/api/challenges/json/age-counting](https://coderbyte.com/api/challenges/json/age-counting). The response contains a data key, and the value is a string with items in the specified format.


# Connect Four Winner

### Description
Connect Four Winner is an exercise about finding a winner in the game of Connect Four on a 6x7 board.

### Solution
The solution for this problem is in the `connect_four_winner_solution.py` file. Make sure to review the code and problem description to understand the implementation.

## How to Contribute
Feel free to contribute additional solutions or improvements to existing code. Your participation is welcome!

## Getting Started
To get started with this repository, clone it to your local machine:

```bash
git clone https://gitlab.com/sofiat9909/challenge-python.git
