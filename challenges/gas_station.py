#Have the function GasStation(strArr) take strArr which will 
#be an an array consisting of the following elements: N which 
#will be the number of gas stations in a circular route and 
#each subsequent element will be the string g:c where g is 
#the amount of gas in gallons at that gas station and c will 
#be the amount of gallons of gas needed to get to the following 
#gas station.

#For example strArr may be: ["4","3:1","2:2","1:2","0:1"]. 
#Your goal is to return the index of the starting gas station 
#that will allow you to travel around the whole route once, 
#otherwise return the string impossible. For the example above, 
#there are 4 gas stations, and your program should return the 
#string 1 because starting at station 1 you receive 3 gallons 
#of gas and spend 1 getting to the next station. Then you have 
#2 gallons + 2 more at the next station and you spend 2 so you 
#have 2 gallons when you get to the 3rd station. You then have 
#3 but you spend 2 getting to the final station, and at the final 
#station you receive 0 gallons and you spend your final gallon 
#getting to your starting point. Starting at any other gas station 
#would make getting around the route impossible, so the answer is 1. 
#If there are multiple gas stations that are possible to start at, 
#return the smallest index (of the gas station). N will be >= 2.


from typing import List
import numpy as np

def GasStation(strArr:List[str]) -> str:
    """
    Calculates the optimal starting gas station index to complete a circular route.

    Args:
    - strArr (List[str]): Array with the number of gas stations and each subsequent element.

    Returns:
    - str: Return index of the optimal starting gas station or "impossible"
           if it's not possible to complete the circular route.
    """
    num_gas_stations = int(strArr[0])

    if num_gas_stations < 2:
        return "impossible"

    try:
        gas_stations = np.array([list(map(int, station.split(':'))) for station in strArr[1:]])

        total_gas = 0
        min_remaining_gas = float('inf')
        start_position = 0

        for i in range(num_gas_stations):
            # Calculate the remaining gas after reaching each station
            total_gas += gas_stations[i, 0] - gas_stations[i, 1]

            # Keep track of the starting position that results in the minimum remaining gas
            if total_gas < min_remaining_gas:
                min_remaining_gas = total_gas
                start_position = (i + 1) % num_gas_stations

        # If there is enough total gas, return the optimal starting position
        if total_gas >= 0:
            return str(start_position + 1)
        else:
            return "impossible"

    except Exception as error:
        raise ValueError(error)

# Example usage:
input_str = ["4", "1:5", "10:3", "3:4", "12:2"]
print(GasStation(input_str))
