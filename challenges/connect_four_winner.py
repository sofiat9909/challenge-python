# Connect Four Winner
#Have the function ConnectFourWinner(strArr) read the strArr 
#parameter being passed which will represent a 6x7 Connect Four board. 
#The rules of the game are: two players alternate turns and place a 
#colored disc down into the grid from the top and the disc falls down 
#the column until it hits the bottom or until it hits a piece beneath it. 
#The object of the game is to connect four of one's own discs of the same 
#color next to each other vertically, horizontally, or diagonally before 
#your opponent. The input strArr will represent a Connect Four board and 
#it will be structured in the following format: ["R/Y","(R,Y,x,x,x,x,x)","(...)","(...)",...)] 
#where R represents the player using red discs, Y represents the 
#player using yellow discs and x represents an empty space on the board. 
#The first element of strArr will be either R or Y and it represents 
#whose turn it is. Your program should determine, based on whose 
#turn it is, whether a space exists that can give that player a win. 
#If a space does exist your program should return the space in the 
#following format: (RxC) where R=row and C=column. If no space exists, 
#return the string none. The board will always be in a legal setup.

#For example, if strArr is: 
#["R","(x,x,x,x,x,x,x)","(x,x,x,x,x,x,x)","(x,x,x,x,x,x,x)","(x,x,x,R,x,x,x)","(x,x,x,R,x,x,x)","(x,x,x,R,Y,Y,Y)"] 
#then your program should return (3x4).

#Another example, if strArr is: 
#["Y","(x,x,x,x,x,x,x)","(x,x,x,x,x,x,x)","(x,x,x,x,x,x,x)","(x,x,Y,Y,x,x,x)","(x,R,R,Y,Y,x,R)","(x,Y,R,R,R,Y,R)"] 
#then your program should return (3x3).



from typing import List
import numpy as np


ROW_COUNT = 6
COLUMN_COUNT = 7

def check_four_sequence( board: np.ndarray, row: int , column: int, direction: tuple, player: str) -> bool:
  """
    Function to check if a player has won in a given direction
    Args : 
    - board :  The connect Four board
    - row (int): Row index of the current position
    - column (int) : Column index of the current position
    - direction (tuple) : A tuple representing the direction to check
    - player (str) : The player ("R" o "Y")

    Returns:
    - bool: True if the player won.
  """

  for i in range(4):
    if board[row + i * direction[0]][column + i * direction[1]] != player:
      return False
  return True

def find_winning_move(board:np.ndarray, player:str) -> bool:
  """
    Function to find winning space

    Args:
    - board (numpy.ndarray) : The connect Four board
    - player (str) : The player ("R" o "Y")

    Returns:
    - bool: True if the player won.
  """
  for row in range(ROW_COUNT):
    for column in range(COLUMN_COUNT):
      if board[row][column] == player:
        if column + 3 < COLUMN_COUNT and check_four_sequence(board,row,column,(0,1),player):
          return True
        if row + 3 < ROW_COUNT and check_four_sequence(board,row,column,(1,0),player):
          return True
        if row - 3 >= 0 and column + 3 < COLUMN_COUNT and check_four_sequence(board,row,column,(-1,1),player):
          return True
        if row + 3 < ROW_COUNT and column + 3 < COLUMN_COUNT and check_four_sequence(board,row,column,(1,1),player):
          return True
  return False


def ConnectFourWinner(strArr:List[str])->str:
  """
  Function that can give that player a win

  Args:
  - strArr (List[str]): Four board representation ["R/Y","(R,Y,x,x,x,x,x)","(...)","(...)",...)] 
    where R represents the player using red discs, Y represents the player using yellow discs and 
    x represents an empty space on the board.

  Returns:
  - str: None if no space exists or (RxC) is space exists. 
  """
  # Extract player's turn
  player_turn = strArr[0]

  if (len(strArr) != ROW_COUNT + 1 or 
      any(len(row.replace('(', '').replace(')', '').split(',')) != COLUMN_COUNT for row in strArr[1:])
    ):
      raise ValueError("Invalid input format")

  # Extract board
  board = (np.array([list(filter(None, row.replace('(', '').replace(')', '').split(','))) for row in strArr[1:]]))
  black_board = 'x'

  #check locations for win
  try:
    for column in range(COLUMN_COUNT):
      for row in range(ROW_COUNT - 1, -1, -1):
        if board[row][column] == black_board:
          board[row][column] = player_turn
          if find_winning_move(board, player_turn):
            return f"({row + 1}x{column+1})"
          board[row][column] = black_board
          break
  except Exception as error:
    raise Exception(error)


  return "none"

# keep this function call here 
print(ConnectFourWinner(input()))
