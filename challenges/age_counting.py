import re
import json
import requests
import numpy as np
import pandas as pd

#print(len(r.json()['data']))

#In the Python file, write a program to perform a GET request on the 
#route https://coderbyte.com/api/challenges/json/age-counting which 
#contains a data key and the value is a string which contains items 
#in the format: key=STRING, age=INTEGER. Your goal is to count how 
#many items exist that have an age equal to or greater than 50, and 
#print this final value.

def count_ages_equal_greater_50(data:str) -> int:
  """
    Function to calculate how many age are equal to or greater than 50.

    Args:
      data (str) : string with format ("key=STRING, age=INTEGER").
      
    Returns:
      int: The count of ages equal to or greater than 50.
  """
  try:
    # Validate input format
    if data is None or not isinstance(data, str):
      raise ValueError("Invalid data format or No data found.")

    # Use a regular expression to directly match and extract dicts.
    json_data = re.findall(r'key=(\w+), age=(\w+)\b', data)

    #validate and filter out invalid ages values
    not_valid_data = [(key,age) for key,age in json_data if not(age.isdigit() and int(age)>=0)]

    if not_valid_data:
      raise ValueError("string invalid format, it should be {key=STRING, age=INTEGER} and age should be >= 0")

    # Create DataFrame with data
    data_frame = pd.DataFrame(json_data, columns=['key','age'])

    #count how many values are equal to or greater than 50
    count_ages_equal_greater_50 = data_frame['age'][data_frame['age'].astype(int) >= 50].shape[0]

    return count_ages_equal_greater_50

  except Exception as error:
    raise Exception(f"Error in data validation {error}")


def main(url:str) -> int:
  """
    Function to perform age counting on data retrieved from a given URL.

    Args:
      url (str): The URL to retrieve data from.
    
    Returns:
      int: The count of ages equal to or greater than 50.


  """
  try:
    # Make a GET request to the provided URL
    response = requests.get(url)
    response.raise_for_status()

    #Extract the data field from the JSON response
    data = response.json().get('data', None)

    #calculated number of ages equal or greater than 50
    age_counting = count_ages_equal_greater_50(data)
    return  age_counting

  except requests.RequestException as error:
    raise ValueError(f"Request Error {error}")



####################################################################
print(main('https://coderbyte.com/api/challenges/json/age-counting'))
